class Cell
  attr_accessor :row, :column, :num_alive_neighbours

  def initialize state, row, col
    @alive      = state
    @row        = row
    @column     = col
    @next_state = @alive
  end

  def prepare_to_mutate!
    die_if_underpopulated
    die_if_overpopulated
    revive_if_born
  end

  def is_alive?; @alive               end
  def mutate!;   @alive = @next_state end
  def die!;      @next_state = false  end
  def revive!;   @next_state = true   end

  def die_if_underpopulated;  die! if num_alive_neighbours < 2      end
  def die_if_overpopulated;   die! if num_alive_neighbours > 3      end
  def revive_if_born;         revive! if num_alive_neighbours == 3  end
end

class Game
  def initialize input_file, iterations, output_file
    file     = File.read(input_file).split("\n")
    @rows    = file.count - 1
    @columns = file.first.split(//).count - 1
    @iterations = iterations.to_i
    @row_low     = 0
    @columns_low = 0
    @grid    = build_from_file file
    @output_file = output_file
  end

  def display
    output_grid = []
    (@row_low..@rows).each do |r_idx|
      o_row = []
      (@columns_low..@columns).each do |c_idx|
        cell = cell_at(r_idx, c_idx)
        o_row << ((cell && cell.is_alive?) ? '1' : '0')
      end
      output_grid << o_row
    end

    [].tap { |output|
      output << output_grid.map{|row| row.map{|cell| cell }.join}
    }.join("\n")
  end

  def start
    @iterations.times do 
      generate
      sleep 0.1
    end

    puts "#{@iterations} iterations have been completed"
    File.open("#{@output_file}", "wb").puts display
  end

  def generate
    @grid.each do |cell|
      cell.num_alive_neighbours = alive_neighbors_of(cell).count
      cell.prepare_to_mutate!
    end

    @grid.map &:mutate!
    @grid.delete_if { |c| !c.is_alive? }
  end

  def cell_at x, y, create_cell = false
    cell = @grid.select {|c| c.row == x && c.column == y}.first
    if cell.nil? and create_cell
      cell = Cell.new(false, x, y)
      @grid << cell

      expand_grid x, y
    end
    cell
  end

  def build_from_file file
    grid = []
    file.each_with_index do |row, r_idx|
      row.split(//).each_with_index do |state, c_idx|
        grid << Cell.new(state == '1', r_idx, c_idx) if state == '1'
      end
    end
    grid
  end

  def alive_neighbors_of cell
    row             = cell.row
    column          = cell.column
    create_new_cell = cell.is_alive?

    neighbours  = []
    neighbours << (cell_at(row - 1, column - 1, create_new_cell))
    neighbours << (cell_at(row - 1, column, create_new_cell))
    neighbours << (cell_at(row - 1, column + 1, create_new_cell))
    neighbours << (cell_at(row, column - 1, create_new_cell))
    neighbours << (cell_at(row, column + 1, create_new_cell))
    neighbours << (cell_at(row + 1, column - 1, create_new_cell))
    neighbours << (cell_at(row + 1, column, create_new_cell))
    neighbours << (cell_at(row + 1, column + 1, create_new_cell))

    neighbours.select{|n| n && n.is_alive?}
  end

  def expand_grid x, y
    @row_low     = x if @row_low > x
    @rows        = x if @rows < x
    @columns_low = y if @columns_low > y
    @columns     = y if @columns < y
  end
end

puts Game.new(ARGV[0], ARGV[2], ARGV[4]).start

# ruby source1.rb input.txt -i 50 -o output.txt